<?php

namespace core;

class App
{

    use TSingletone;

    protected function __construct()
    {
        $query = trim($_SERVER['QUERY_STRING'], '/');
        session_start();
        new ErrorHandler();
        Router::dispatch($query);

    }

}
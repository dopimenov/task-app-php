<?php

namespace core;

use Exception;
use \RedBeanPHP\R as R;

class Db
{

    use TSingletone;

    protected function __construct()
    {
        $config_db = require CONF . '/config_db.php';
        if (file_exists(CONF . '/db.php')) {
            $db = require CONF . '/db.php';
            $db = array_merge($config_db, $db);
        }
        class_alias('\RedBeanPHP\R', '\R');
        R::setup($db['dsn'], $db['user'], $db['pass']);
        if (!R::testConnection()) {
            throw new Exception("Нет соединения с БД", 500);
        }
        R::freeze(true);
        if (DEBUG) {
            R::debug(true, 1);
        }

        R::ext('xdispense', function ($type) {
            return R::getRedBean()->dispense($type);
        });
    }

}
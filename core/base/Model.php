<?php

namespace core\base;

use core\Db;
use \R;
use RedBeanPHP\OODBBean;
use Valitron\Validator;

class Model
{

    public $attributes = [];
    public $errors = [];
    public $rules = [];

    public function __construct()
    {
        Db::instance();
    }

    public function load($data)
    {
        foreach ($this->attributes as $name => $value) {
            if (isset($data[$name])) {
                $this->attributes[$name] = $data[$name];
            }
        }
    }

    public function save($table, $valid = true)
    {
        if ($valid) {
            $tbl = R::dispense($table);
        } else {
            $tbl = R::xdispense($table);
        }
        foreach ($this->attributes as $name => $value) {
            $tbl->$name = $value;
        }
        return R::store($tbl);
    }

    public function update(OODBBean $bean)
    {

        foreach ($this->attributes as $name => $value) {
            $bean->$name = $value;
        }
        return R::store($bean);
    }

    public function validate($data)
    {
        Validator::langDir(WWW . '/validator/lang');
        Validator::lang('ru');
        $v = new Validator($data);
        $v->rules($this->rules);
        if ($v->validate()) {
            return true;
        }
        $this->errors = $v->errors();
        return false;
    }

}
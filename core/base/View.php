<?php

namespace core\base;

use Exception;

class View
{

    public $route;
    public $controller;
    public $view;
    public $layout;

    public function __construct($route, $layout = '', $view = '')
    {
        $this->route = $route;
        $this->controller = $route['controller'];
        $this->view = $view;

        if ($layout === false) {
            $this->layout = false;
        } else {
            $this->layout = $layout ?: LAYOUT;
        }
    }

    public function render($data)
    {
        if (is_array($data)) extract($data);
        $viewFile = APP . "/views/{$this->controller}/{$this->view}.php";
        if (!Controller::isAjax()) {
            if (is_file($viewFile)) {
                ob_start();
                require_once $viewFile;
                $content = ob_get_clean();
            } else {
                throw new Exception("Не найден вид {$viewFile}", 500);
            }
        }
        if (false !== $this->layout) {
            $layoutFile = APP . "/views/layouts/{$this->layout}.php";
            if (is_file($layoutFile)) {
                require_once $layoutFile;
            } else {
                throw new Exception("Не найден шаблон {$this->layout}", 500);
            }
        }
    }


}
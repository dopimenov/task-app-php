<?php

namespace core\base;

class Controller
{

    public $route;
    public $controller;
    public $view;
    public $layout;
    public $data = [];

    public function __construct($route)
    {
        $this->route = $route;
        $this->controller = $route['controller'];
        $this->view = $route['action'];
    }

    public function getView()
    {
        $viewObject = new View($this->route, $this->layout, $this->view);
        $viewObject->render($this->data);
    }

    public function set($data)
    {
        $this->data = $data;
    }

    public static function isAjax()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
    }

    public function getErrors($errors)
    {
        if(empty($errors)) return '';
        $html = '<ul>';
        foreach ($errors as $error) {
            foreach ($error as $item) {
                $html .= "<li>$item</li>";
            }
        }
        $html .= '</ul>';
        $_SESSION['error'] = $html;
        return $errors;
    }

}
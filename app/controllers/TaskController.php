<?php

namespace app\controllers;

use app\models\Task;
use app\models\User;
use core\libs\Pagination;
use \R;

class TaskController extends AppController
{

    const STATUS_NEW = 'Новая';
    const STATUS_WORK = 'В работе';
    const STATUS_READY = 'Выполнено';
    const PAGE_PER_PAGE = 3;

    public static $status = [
        0 => self::STATUS_NEW,
        1 => self::STATUS_WORK,
        2 => self::STATUS_READY
    ];

    public function indexAction()
    {
        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $order = isset($_GET['order']) ? $_GET['order'] : '';
        $order_by = isset($_GET['order_by']) ? $_GET['order_by'] : '';

        $order = ($order == 'ASC') ? 'ASC' : 'DESC';

        $allowed_order_by = array("name", "title", 'id', 'email', 'user_email', 'created_at', 'updated_at', 'status');
        $key_order_by = array_search($order_by, $allowed_order_by);

        if ($key_order_by === false)
            $order_by = 'id';
        else
            $order_by = $allowed_order_by[$key_order_by];

        $count = R::count('task');
        $perpage = $page == 'all' ? $count : self::PAGE_PER_PAGE;

        $pagination = new Pagination($page, $perpage, $count);
        $start = $pagination->getStart();

        $_order_by = ($order_by == 'user_email') ? 'user.email' : $order_by;
        $tasks = R::findAll('task', "JOIN user ON user.id = task.user_id ORDER BY $_order_by $order LIMIT $perpage OFFSET $start ");

        $users = R::findAll('user');
        $is_admin = (isset($_SESSION['user']['role']) AND $_SESSION['user']['role'] == 'admin') ? true : false;

        $show_all_link = $this->add_or_update_params('page', 'all');
        $paginate_link = $this->removeParam('page');

        $this->set(compact('tasks', 'users', 'count', 'is_admin', 'pagination', 'order_by', 'order', 'show_all_link','paginate_link'));
    }

    public function createAction()
    {
        if (self::isAjax())
            header('Content-Type:application/json; charset=utf-8');

        $form_data = $_POST;
        $form_data['created_at'] = time();
        $form_data['status'] = array_search(self::STATUS_NEW, self::$status);

        $task = new Task();
        $task->load($form_data);

        if (!$task->validate($form_data) OR $task->checkEmpty()) {
            $_SESSION['form_data'] = $form_data;
            $errors = $this->getErrors($task->errors);
            if (self::isAjax()) {
                unset($_SESSION['error']);
                echo json_encode(['errors' => $errors]);
            } else redirect(PATH);
            exit();
        }

        if ($id = $task->save('task')) {
            unset($_SESSION['error']);
            $_SESSION['success'] = "Задача #$id создана";
            if (self::isAjax()) echo json_encode(['success' => 1]);
        } else {
            $_SESSION['error'] = 'Ошибка!';
            if (self::isAjax()) echo json_encode(['success' => 0]);
        }

        if (self::isAjax())
            exit();
        else
            redirect(PATH);
    }

    public function viewAction($id)
    {
        $task = Task::findModel($id);
        $this->set(compact('task'));
    }

    public function updateAction($id)
    {
        if (!User::checkAuth()) {
            redirect(PATH . '/user/login');
            exit();
        }

        $task = Task::findModel($id);
        $form_data = $_POST;

        if (empty($form_data)) {
            $users = R::findAll('user');
            $this->set(compact('task', 'users'));
            return;
        }

        $form_data['created_at'] = $task->created_at;
        $form_data['updated_at'] = ($task->description == $form_data['description'] AND empty($task->updated_at)) ? null : time();

        $_task = new Task();
        $_task->load($form_data);

        if (!$_task->validate($form_data) OR $_task->checkEmpty()) {
            $this->getErrors($_task->errors);
            $_SESSION['form_data'] = $form_data;
        } else {
            $_task->update($task);
            $_SESSION['success'] = "Задача #$id обновлена ";
        }
        redirect(PATH . '/task/update?id=' . $id);
        exit();
    }

    public function deleteAction($id)
    {
        if (!User::checkAuth()) {
            redirect(PATH . '/user/login');
            exit();
        }

        $task = Task::findModel($id);
        R::trash($task);

        $_SESSION['success'] = "Задача #$id удалена";
        redirect(PATH);
        exit();
    }
}
<?php

namespace app\controllers;

use app\models\User;

class UserController extends AppController
{

    public function signupAction()
    {

        if (!empty($_POST)) {
            $user = new User();
            $form_data = $_POST;
            $user->load($form_data);

            if (!$user->validate($form_data) || !$user->checkUnique()) {
                $this->getErrors($user->errors);
                $_SESSION['form_data'] = $form_data;
            } else {
                $user->attributes['password'] = password_hash($user->attributes['password'], PASSWORD_DEFAULT);
                if ($user->save('user')) {
                    $_SESSION['success'] = 'Пользователь зарегистрирован успешно. Пожалуйста, выполните вход.';
                    redirect(PATH . '/user/login');
                } else {
                    $_SESSION['error'] = 'Ошибка!';
                }
            }
            redirect(PATH . '/user/signup');
        }
    }

    public function loginAction()
    {
        $login = isset($_POST['login']) ? trim($_POST['login']) : null;
        $password = isset($_POST['password']) ? trim($_POST['password']) : null;

        if ($login !== null AND  $password !== null) {
            $user = new User();
            if ($user->login($login, $password)) {
                $_SESSION['success'] = 'Вы успешно авторизованы';
                redirect(PATH);
            } else {
                $_SESSION['error'] = 'Логин/пароль введены неверно';
                redirect(PATH . '/user/login');
            }
        }
    }

    public function logoutAction()
    {
        if (isset($_SESSION['user'])) unset($_SESSION['user']);
        redirect();
    }

}
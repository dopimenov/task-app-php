<?php

namespace app\controllers;

use core\base\Controller;
use core\Db;

class AppController extends Controller
{

    public $layout = 'default';

    public function __construct($route)
    {
        parent::__construct($route);
        Db::instance();

        $this->generateToken();

        if(!empty($_POST)):
            if(!isset($_POST['token']) OR !$this->checkToken($_POST['token'])){
                $error_message = 'Неверный CSRF токен. Попробуйте обновить страницу.';
                $_SESSION['error'] = $error_message;
                if (self::isAjax()){
                    unset($_SESSION['error']);
                    echo json_encode(['errors' => ['token'=>$error_message]]);
                }else
                    redirect(PATH);
                exit();
            }
        endif;

    }

    public function generateToken() {
        if(!isset($_SESSION['token']))
            $_SESSION['token'] = base64_encode(openssl_random_pseudo_bytes(32));
    }

    public function checkToken($token){
        if(isset($_SESSION['token']) && $token === $_SESSION['token'])
            return true;
        return false;
    }

    public function getUrl()
    {
        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }

    public function add_or_update_params($key, $value)
    {
        $url = $this->getUrl();
        $url = parse_url($url);
        $query = isset($url['query']) ? $url['query'] : '';
        parse_str($query, $params);
        $params[$key] = $value;
        $query = http_build_query($params);
        $result = '';
        if ($url['scheme']) {
            $result .= $url['scheme'] . ':';
        }
        if ($url['host']) {
            $result .= '//' . $url['host'];
        }
        if ($url['path']) {
            $result .= $url['path'];
        }
        if ($query) {
            $result .= '?' . $query;
        }
        return $result;
    }

    public function removeParam($param)
    {
        $url = $this->getUrl();
        $query = parse_url($url,PHP_URL_QUERY);
        parse_str($query,$vars);
        $url = strtok($url, '?') ;
        $vars = array_diff_key($vars,['page'=>'']);
        $url .= !empty($vars)?'?'.http_build_query($vars):'';
        return $url;
    }

}
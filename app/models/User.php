<?php

namespace app\models;

use \R;
use core\base\Model;

class User extends Model
{

    public $attributes = [
        'login' => '',
        'password' => '',
        'name' => '',
        'email' => '',
        'role' => 'user',
    ];

    public $rules = [
        'required' => [
            ['login'],
            ['password'],
            ['name'],
            ['email'],
        ],
        'email' => [
            ['email'],
        ],
        'lengthMin' => [
            ['password', 3],
        ]
    ];

    public function checkUnique()
    {
        $user = R::findOne('user', 'login = ? OR email = ?', [$this->attributes['login'], $this->attributes['email']]);
        if ($user) {
            if ($user->login == $this->attributes['login']) {
                $this->errors['unique'][] = 'Этот логин уже занят';
            }
            if ($user->email == $this->attributes['email']) {
                $this->errors['unique'][] = 'Этот email уже занят';
            }
            return false;
        }
        return true;
    }

    public function login($login, $password, $isAdmin = false)
    {
        if ($login && $password) {
            if ($isAdmin) {
                $user = R::findOne('user', "login = ? AND role = 'admin'", [$login]);
            } else {
                $user = R::findOne('user', "login = ?", [$login]);
            }
            if ($user) {
                if (password_verify($password, $user->password)) {
                    foreach ($user as $k => $v) {
                        if ($k != 'password') $_SESSION['user'][$k] = $v;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public static function checkAuth()
    {
        return (isset($_SESSION['user']) && $_SESSION['user']['role'] == 'admin');
    }

}
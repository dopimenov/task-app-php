<?php

namespace app\models;

use core\base\Model;

class Task extends Model
{

    public $attributes = [
        'title' => '',
        'description' => '',
        'user_id' => '',
        'created_at' => '',
        'updated_at' => null,
        'status' => '',
        'email' => '',
    ];

    public $rules = [
        'required' => [
            ['title'],
            ['description'],
            ['email'],
            ['user_id'],
        ],
        'email' => [
            ['email'],
        ],
    ];

    public function checkEmpty()
    {
        $flag = false;
        if ($this->attributes['title'] == '') {
            $this->errors['title'][] = 'Название задачи пусто';
            $flag = true;
        }
        if ($this->attributes['description'] == '') {
            $this->errors['description'][] = 'Описание задачи пусто';
            $flag = true;
        }
        if ($this->attributes['user_id'] == '') {
            $this->errors['user_id'][] = 'Пользователь не задан';
            $flag = true;
        }
        if ($this->attributes['email'] == '') {
            $this->errors['email'][] = 'Не введен email';
            $flag = true;
        }
        return $flag;
    }
    
    public static function findModel($id)
    {
        if ($task = \R::findOne('task', "id = ?", [$id]))
            return $task;

        throw new Exception("Задача с #$id не найдена", 404);
    }

}
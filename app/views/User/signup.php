<div class="register-main">
    <form method="post" class="form-signin" action="<?= PATH; ?>/user/signup" id="signup" role="form"
          data-toggle="validator">
        <div class="form-group has-feedback">
            <label for="login">Логин</label>
            <input type="text" name="login" class="form-control" id="login" placeholder="Login"
                   value="<?= isset($_SESSION['form_data']['login']) ? h($_SESSION['form_data']['login']) : ''; ?>"
                   required>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group has-feedback">
            <label for="pasword">Пароль</label>
            <input type="password" name="password" class="form-control" id="pasword" placeholder="Password"
                   data-error="Пароль должен включать не менее 6 символов" data-minlength="6"
                   value="<?= isset($_SESSION['form_data']['password']) ? h($_SESSION['form_data']['password']) : ''; ?>"
                   required>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>
        </div>
        <div class="form-group has-feedback">
            <label for="name">Имя</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Имя"
                   value="<?= isset($_SESSION['form_data']['name']) ? h($_SESSION['form_data']['name']) : ''; ?>"
                   required>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group has-feedback">
            <label for="email">Email</label>
            <input name="email" class="form-control" id="email" placeholder="Email"
                   value="<?= isset($_SESSION['form_data']['email']) ? h($_SESSION['form_data']['email']) : ''; ?>"
                   required>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <input type="hidden" name="token" value="<?=isset($_SESSION['token'])?$_SESSION['token']:null;?>">
        <button type="submit" class="btn btn-lg btn-primary btn-block">Зарегистрировать</button>
    </form>
    <?php if (isset($_SESSION['form_data'])) unset($_SESSION['form_data']); ?>
</div>

<div class="register-main">
    <form class="form-signin" method="post" action="<?= PATH; ?>/user/login" id="login" role="form">
        <h2 class="form-signin-heading">Вход</h2>
        <div class="form-group has-feedback">
            <label for="inputLogin" class="sr-only">Логин</label>
            <input type="text" name="login" class="form-control" id="inputLogin" placeholder="Login" required>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group has-feedback">
            <label for="inputPassword" class="sr-only">Пароль</label>
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password"
                   required="">
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        </div>
        <input type="hidden" name="token" value="<?=isset($_SESSION['token'])?$_SESSION['token']:null;?>">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Ввойти</button>
    </form>
</div>



<?php
/* @var $content string */
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Приложение-задачник</title>
    <link rel="icon" type="image/png" href="<?= PATH; ?>/img/favicon.ico">
    <link href="<?= PATH; ?>/libs/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"
          media="all"/>
    <link href="<?= PATH; ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?= PATH; ?>/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <script src="<?= PATH; ?>/js/jquery.min.js"></script>
    <script src="<?= PATH; ?>/js/bootstrap.min.js"></script>
    <script src="<?= PATH; ?>/js/script.js"></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var myAjax = {"ajaxurl": "<?=PATH?>"};
        /* ]]> */
    </script>

</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-left">
            <a class="navbar-brand" href="<?= PATH; ?>/">TASKS</a>
        </div>
        <div class="navbar-right user_info">
            <ul>
                <?php if (!empty($_SESSION['user'])): ?>
                    <li>Добро пожаловать, <?= h($_SESSION['user']['name']); ?></li>
                    <li><a class="dropdown-item" href="<?= PATH; ?>/user/logout">Выход</a></li>
                <?php else: ?>
                    <li><a class="dropdown-item" href="<?= PATH; ?>/user/login">Вход</a></li>
                    <li><a class="dropdown-item" href="<?= PATH; ?>/user/signup">Регистрация</a></li>
                <?php endif; ?>
            </ul>
        </div>

    </div>
</nav>

<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php if (isset($_SESSION['error'])): ?>
                    <div class="alert alert-danger">
                        <?php echo $_SESSION['error'];
                        unset($_SESSION['error']); ?>
                    </div>
                <?php endif; ?>
                <?php if (isset($_SESSION['success'])): ?>
                    <div class="alert alert-success">
                        <?php echo $_SESSION['success'];
                        unset($_SESSION['success']); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="container">
        <?= $content ?>
    </div>
</div>

</body>
</html>


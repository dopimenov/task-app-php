<?php
/* @var $task OODBBean */
/* @var $users array */
/* @var $user OODBBean */

$props = $task->getProperties();

use app\controllers\TaskController;
use RedBeanPHP\OODBBean; ?>

<div class="row">
    <div class="col-sm-12">
        <form method="POST" action="<?= PATH; ?>/task/update?id=<?= $props['id'] ?>">
            <div class="modal-body">
                <h2 class="modal-title">Обновить задачу</h2>
                <div class="form-group required">
                    <label for="InputTitle">Название</label>
                    <input type="text" name="title" value="<?= isset($props['title']) ? $props['title'] : '' ?>"
                           class="form-control" id=InputTitle" aria-describedby="emailTitle"
                           placeholder="Введите название">
                </div>
                <div class="form-group required">
                    <label for="InputDescription">Описание</label>
                    <textarea name="description" class="form-control" rows="7" id="InputDescription"
                              aria-describedby="InputDescription"
                              placeholder="Введите описание"><?= isset($props['description']) ? $props['description'] : '' ?></textarea>
                </div>
                <div class="form-group required">
                    <label for="InputEmail">Email</label>
                    <input class="form-control" name="email"
                           value="<?= isset($props['email']) ? $props['email'] : '' ?>" id="InputEmail"
                           aria-describedby="InputEmail" placeholder="Введите email">
                </div>
                <div class="form-group">
                    <label for="RespSelect">Пользователь</label>
                    <select name="user_id" class="form-control" id="RespSelect">
                        <?php foreach ($users as $user): ?>
                            <option <?= ((int)$props['user_id'] == $user->id) ? 'selected' : ''; ?>
                                    value="<?= $user->id ?>"><?= $user->name . ' ' . $user->email ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="statusSelect">Статус</label>
                    <?php $statuses = array_flip(TaskController::$status); ?>
                    <select name="status" class="form-control" id="statusSelect">
                        <option <?= $props['status'] == $statuses[TaskController::STATUS_NEW] ? 'selected' : '' ?>
                                value="<?= $statuses[TaskController::STATUS_NEW] ?>"><?= TaskController::STATUS_NEW ?></option>
                        <option <?= $props['status'] == $statuses[TaskController::STATUS_WORK] ? 'selected' : '' ?>
                                value="<?= $statuses[TaskController::STATUS_WORK] ?>"><?= TaskController::STATUS_WORK ?></option>
                        <option <?= $props['status'] == $statuses[TaskController::STATUS_READY] ? 'selected' : '' ?>
                                value="<?= $statuses[TaskController::STATUS_READY] ?>"><?= TaskController::STATUS_READY ?></option>
                    </select>
                </div>
                <input type="hidden" name="token" value="<?=isset($_SESSION['token'])?$_SESSION['token']:null;?>">
                <a href="<?= PATH; ?>/" type="button" class="btn btn-default">Назад</a>
                <button type="submit" class="btn btn-primary">Обновить</button>
            </div>
        </form>
    </div>
</div>

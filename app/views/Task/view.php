<?php
/* @var $task OODBBean */

use app\controllers\TaskController;
use RedBeanPHP\OODBBean;

$props = $task->getProperties();
$titles = ['id', 'Название', 'Описание', 'Статус', 'Email', 'Пользователь', 'Создано', 'Описание обновлено'];
if (count($props) == count($titles)): $props = array_combine($titles, $props); ?>

    <h2 class="modal-title">Просмотр задачи</h2>
    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th width="20%" scope="col">Поле</th>
            <th scope="col">Содержимое</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($props as $key => $value): ?>
            <tr>
                <th scope="row"><?= $key ?></th>
                <?php if ($key == 'Статус'): ?>
                    <td><?= isset(TaskController::$status[$value]) ? TaskController::$status[$value] : '' ?></td>
                <?php elseif ($key == 'Пользователь'): ?>
                    <td><?= $task->user->name . ', ' . $task->user->email ?></td>
                <?php elseif ($key == 'Создано'): ?>
                    <td><?= isset($task->created_at) ? date('d-m-Y H:i', $task->created_at) : '' ?></td>
                <?php elseif ($key == 'Описание обновлено'): ?>
                    <td><?= isset($task->updated_at) ? 'Отредактировано администратором' : '' ?></td>
                <?php else: ?>
                    <td><?= h($value) ?></td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <a href="<?= PATH; ?>/" type="button" class="btn btn-default">Назад</a>

<?php endif; ?>
<?php
/* @var $pagination Pagination */
/* @var $users array */
/* @var $count int */
/* @var $is_admin bool */
/* @var $order_by string */
/* @var $order string */
/* @var $show_all_link string */
/* @var $paginate_link string */
/* @var $tasks array */

use app\controllers\TaskController;
use core\libs\Pagination;
use RedBeanPHP\OODBBean; ?>
<section class="content">
    <div class="row">
        <div class="col-sm-3 create-row">
            <a href="#creator_task" id="creator_task_button" class="btn btn-success" data-toggle="modal">Создать
                задачу</a>
        </div>
        <div class="col-sm-3 create-row">
            <a href="<?= $show_all_link ?>" id="creator_task_button" class="btn btn-primary <?=($count<=$pagination->perpage)?'hide':''?>">
                Показать все</a>
            <a href="<?=$paginate_link?>" id="creator_task_button" class="btn btn-primary <?=($count!==$pagination->perpage OR $count <= TaskController::PAGE_PER_PAGE )?'hide':''?>">
                Показать по (<?=TaskController::PAGE_PER_PAGE?>) на странице</a>
        </div>
        <div class="col-sm-3">
            <div class="sortable <?=($count<=1)?'hide':''?>">
                <div class="form-group">
                    <label for="sortSelect">Сортировать по:</label>
                    <select name="order_by" class="form-control" id="sortSelect" value="<?= $order_by ?>">
                        <option <?= ($order_by == 'name') ? 'selected' : '' ?> value="name">Имени</option>
                        <option <?= ($order_by == 'title') ? 'selected' : '' ?> value="title">Названию</option>
                        <option <?= ($order_by == 'email') ? 'selected' : '' ?> value="email">Email</option>
                        <option <?= ($order_by == 'user_email') ? 'selected' : '' ?> value="user_email">Email
                            пользователя
                        </option>
                        <option <?= ($order_by == 'created_at') ? 'selected' : '' ?> value="created_at">Создано</option>
                        <option <?= ($order_by == 'updated_at') ? 'selected' : '' ?> value="updated_at">Обновлению
                            описания
                        </option>
                        <option <?= ($order_by == 'status') ? 'selected' : '' ?> value="status">Статусу</option>
                        <option <?= ($order_by == 'id') ? 'selected' : '' ?> value="id">Идентификатору</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="sortable <?=($count<=1)?'hide':''?>">
                <div class="form-group">
                    <label for="orderSelect">Направление:</label>
                    <select name="order" class="form-control" id="orderSelect" value="<?= $order ?>">
                        <option <?= ($order == 'ASC') ? 'selected' : '' ?> value="ASC">Возрастанию</option>
                        <option <?= ($order == 'DESC') ? 'selected' : '' ?> value="DESC">Убыванию</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Список задач</h3>
                </div>
                <div class="panel-body">
                    <?php if (!empty($tasks)): ?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Название</th>
                                    <th>Описание</th>
                                    <th>Email</th>
                                    <th>Имя пользователя</th>
                                    <th>Email пользователя</th>
                                    <th>Статус</th>
                                    <th>Создано</th>
                                    <th>Описание обновлено</th>
                                    <th>Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php /* @var $task OODBBean */
                                foreach ($tasks as $task): ?>
                                    <tr>
                                        <td><?= $task['id']; ?></td>
                                        <td><?= h($task['title']); ?></td>
                                        <td><?php $description = h($task['description']);
                                            if (strlen($description) > 120)
                                                echo substr($description, 0, 120);
                                            else echo $description;
                                            ?></td>
                                        <td><?= h($task['email']); ?></td>
                                        <td><?= $task->user->name ?></td>
                                        <td><?= $task->user->email ?></td>
                                        <td><?= isset(TaskController::$status[$task['status']]) ? TaskController::$status[$task['status']] : '' ?></td>
                                        <td><?= date('d-m-Y H:i', $task['created_at']); ?></td>
                                        <td><?= isset($task['updated_at']) ? 'Отредактировано администратором' : '' ?></td>
                                        <td>
                                            <a id="eye-<?= $task['id']; ?>" class="popoverHandler-ajax"
                                               data-placement="top" data-content="Просмотреть" data-original-title=""
                                               href="<?= PATH ?>/task/view?id=<?= $task['id']; ?>"><i
                                                        class="fa fa-eye fa-fw"></i></a>
                                            <?php if ($is_admin): ?>
                                                <a id="edit-<?= $task['id']; ?>" class="popoverHandler-ajax"
                                                   data-placement="top" data-content="Редактировать"
                                                   data-original-title=""
                                                   href="<?= PATH ?>/task/update?id=<?= $task['id']; ?>"><i
                                                            class="fa fa-edit fa-fw"></i></a>
                                                <a id="trash-o-<?= $task['id']; ?>" class="popoverHandler-ajax"
                                                   data-placement="top" data-content="Удалить" data-original-title=""
                                                   href="<?= PATH ?>/task/delete?id=<?= $task['id']; ?>"><i
                                                            class="fa fa-trash-o fa-fw"></i></a>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center">
                            <p>Показано (<?= count($tasks); ?> задач из <?= $count; ?>)</p>
                            <?php if ($pagination->countPages > 1): ?>
                                <?= $pagination; ?>
                            <?php endif; ?>
                        </div>
                    <?php else: ?>
                        <h3 class="text-center">Список задач пуст</h3>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </div>
</section>

<div id="creator_task" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="form_task-create" method="POST" action="<?= PATH; ?>/task/create">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Создать задачу</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group required">
                        <label for="InputTitle">Название</label>
                        <input type="text" name="title" class="form-control" id=InputTitle"
                               aria-describedby="emailTitle" placeholder="Введите название">
                        <span class="errors-text"></span>
                    </div>
                    <div class="form-group required">
                        <label for="InputDescription">Описание</label>
                        <textarea class="form-control" name="description" rows="7" id="InputDescription"
                                  aria-describedby="InputDescription" placeholder="Введите описание"></textarea>
                        <span class="errors-text"></span>
                    </div>
                    <div class="form-group required">
                        <label for="InputEmail">Email</label>
                        <input class="form-control" name="email" id="InputEmail" aria-describedby="InputEmail"
                               placeholder="Введите email">
                        <span class="errors-text"></span>
                    </div>
                    <div class="form-group">
                        <label for="RespSelect">Пользователь</label>
                        <select class="form-control" name="user_id" id="RespSelect">
                            <?php
                            /* @var $user OODBBean */
                            foreach ($users as $user):?>
                                <option <?= ('admin' == $user->role) ? 'selected' : ''; ?>
                                        value="<?= $user->id ?>"><?= $user->name . ' ' . $user->email ?></option>
                            <?php endforeach; ?>
                        </select>
                        <span class="errors-text"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="token" value="<?=isset($_SESSION['token'])?$_SESSION['token']:null;?>">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="submit" class="btn btn-primary" id="create-task">Создать</button>
                </div>
            </form>
        </div>
    </div>
</div>
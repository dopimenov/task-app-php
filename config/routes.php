<?php

use core\Router;

Router::add('^$', ['controller' => 'Task', 'action' => 'index']);
Router::add('^(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$');
$(document).ready(function() {
    const   creator_task = $('#creator_task');
    const   title =  creator_task.find('input[name=title]'),
        description =  creator_task.find('textarea[name=description]'),
        email =   creator_task.find('input[name=email]'),
        token =   creator_task.find('input[name=token]');

    $('#orderSelect').on('change', function(){
        let order = $('#orderSelect :selected').val();
        let url = new URL(window.location.href);
        url.searchParams.set('order', order);
        window.location.href = url.href;
    });

    $('#sortSelect').on('change', function(){
        let sort = $(this).val();
        let url = new URL(window.location.href);
        url.searchParams.set('order_by', sort);
        window.location.href = url.href;
    });

    $('#form_task-create').on('submit', function(e){
        e.preventDefault();
        clear_data(title,description,email);
        let user_id = creator_task.find('select[name=user_id]');

        $.ajax({
            url: myAjax.ajaxurl+'/task/create',
            data: {
                title:title.val(),
                description:description.val(),
                user_id:user_id.find(':selected').val(),
                email:email.val(),
                token:token.val()
            },
            type: 'POST',
            dataType: 'json',
            success: function(jsonData) {
                if (jsonData.success == "1") {
                    location.href = myAjax.ajaxurl;
                } else {
                    var message = [];
                    if(jsonData.errors.email){
                        message = [];
                        jsonData.errors.email.map(function (item) {
                            message.push('<li>'+item+'</li>');
                        });
                        email.next().html('<ul>'+message.join('')+'</ul>');
                    }
                    if(jsonData.errors.title){
                        message = [];
                        jsonData.errors.title.map(function (item) {
                            message.push('<li>'+item+'</li>');
                        });
                        title.next().html('<ul>'+message.join('')+'</ul>');
                    }
                    if(jsonData.errors.description){
                        message = [];
                        jsonData.errors.description.map(function (item) {
                            message.push('<li>'+item+'</li>');
                        });
                        description.next().html('<ul>'+message.join('')+'</ul>');
                    }
                    if(jsonData.errors.user_id){
                        message = [];
                        jsonData.errors.user_id.map(function (item) {
                            message.push('<li>'+item+'</li>');
                        });
                        user_id.next().html('<ul>'+message.join('')+'</ul>');
                    }
                    if(jsonData.errors.token){
                        alert(jsonData.errors.token);
                    }else {
                        if(title.val() == '') title.addClass('errors');
                        if(description.val() == '') description.addClass('errors');
                        if(email.val() == '') email.addClass('errors');
                        if(user_id.find(':selected').val() == undefined) user_id.addClass('errors');
                    }
                }
            },
            error: function(resp){
                alert('Error!');
                console.log(resp)
            }
        });
    });

    $(creator_task).on('hidden.bs.modal', function () {
        clear_data(title,description,email);
    });

    function clear_data(title,description,email){
        title.removeClass('errors'); title.next().html('');
        description.removeClass('errors'); description.next().html('');
        email.removeClass('errors'); email.next().html('');
    }
});
